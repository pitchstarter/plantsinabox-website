/**
 * Why in a box popup
 * **/
function popup() {
    $.magnificPopup.open({
        items: {
            src: '.popup'
        },
        type: 'inline'
    }, 0);
    $.fn.matchHeight._update();
}

/**
 * If only single variant, hide toggle icons
 * **/
function singleProductNav() {
    var varient_count = $('.desktop--addcart input[type="radio"]').length;

    if (varient_count === 1) {
        $('#up, #down').css("display", "none");
    }
}

/**
 * Desktop | Top add to cart plus | toggle logic
 * **/
function desktopAddcartUp(e) {
    e.preventDefault();
    $(".desktop--addcart input[type='radio']:checked").nextAll('input').eq(0).prop("checked", true);
    if ($(".desktop--addcart input[type='radio']:checked").prop('disabled') == true) {
        $(".desktop--addcart button[type='submit']").prop('disabled', true).html("Sold Out");
    } else {
        $(".desktop--addcart button[type='submit']").prop('disabled', false).html("Add to Cart").append(' <img src="//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/addtocart.svg" />');
    }

    if ($('.desktop--addcart #down + input[type="radio"]:checked').length != 1) {
        $('.desktop--addcart #down').removeClass('disable');
    }

}

/**
 * Desktop | Top add to cart remove | toggle logic
 * **/
function desktopAddcartDown(e) {
    e.preventDefault();
    $(".desktop--addcart input[type='radio']:checked").prevAll().eq(1).prop("checked", true);
    if ($(".desktop--addcart input[type='radio']:checked").prop('disabled') == true) {
        $(".desktop--addcart button[type='submit']").prop('disabled', true).html("Sold Out");
    } else {
        $(".desktop--addcart button[type='submit']").prop('disabled', false).html("Add to Cart").append(' <img src="//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/addtocart.svg" />');
    }
    if ($('.desktop--addcart #down + input[type="radio"]:checked').length == 1) {
        $('.desktop--addcart #down').addClass('disable');
    }

}

/**
 * Mobile | Top add to cart add | toggle logic
 * **/

function mobileAddcartUp(e) {
    e.preventDefault();
    $(".mobile--addcart input[type='radio']:checked").nextAll('input').eq(0).prop("checked", true);
    if ($(".mobile--addcart input[type='radio']:checked").prop('disabled') == true) {
        $(".mobile--addcart button[type='submit']").prop('disabled', true).html("Sold Out");
    } else {
        $(".mobile--addcart button[type='submit']").prop('disabled', false).html("Add to Cart").append(' <img src="//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/addtocart.svg" />');
    }

    if ($('.mobile--addcart #down + input[type="radio"]:checked').length != 1) {
        $('.mobile--addcart #down').removeClass('disable');
    }

}

/**
 * Mobile | Top add to cart remove | toggle logic
 * **/
function mobileAddcartDown(e) {
    e.preventDefault();
    $(".mobile--addcart input[type='radio']:checked").prevAll().eq(1).prop("checked", true);
    if ($(".mobile--addcart input[type='radio']:checked").prop('disabled') === true) {
        $(".mobile--addcart button[type='submit']").prop('disabled', true).html("Sold Out");
    } else {
        $(".mobile--addcart button[type='submit']").prop('disabled', false).html("Add to Cart").append(' <img src="//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/addtocart.svg" />');
    }

    if ($('.mobile--addcart #down + input[type="radio"]:checked').length == 1) {
        $('.mobile--addcart #down').addClass('disable');
    }
}

/**
 * Desktop | Bottom add to cart add | toggle logic
 * **/

function desktopCtacartUp(e) {
    e.preventDefault();
    $(".buy__cta input[type='radio']:checked").nextAll('input').eq(0).prop("checked", true);
    if ($(".buy__cta input[type='radio']:checked").prop('disabled') == true) {
        $(".buy__cta button[type='submit']").prop('disabled', true).html("Sold Out");
    } else {
        $(".buy__cta button[type='submit']").prop('disabled', false).html("Add to Cart").append(' <img src="//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/addtocart.svg" />');
    }

    if ($('.buy__cta #down + input[type="radio"]:checked').length != 1) {
        $('.buy__cta #down').removeClass('disable');
    }
}

/**
 * Desktop | Bottom add to cart remove | toggle logic
 * **/
function desktopCtacartDown(e) {
    e.preventDefault();
    $(".buy__cta input[type='radio']:checked").prevAll().eq(1).prop("checked", true);
    if ($(".buy__cta input[type='radio']:checked").prop('disabled') == true) {
        $(".buy__cta button[type='submit']").prop('disabled', true).html("Sold Out");
    } else {
        $(".buy__cta button[type='submit']").prop('disabled', false).html("Add to Cart").append(' <img src="//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/addtocart.svg" />');
    }
    if ($('.buy__cta #down + input[type="radio"]:checked').length == 1) {
        $('.buy__cta #down').addClass('disable');
    }
}

/**
 * Remove clickable link and add attribute to 'Why in a box' in menu
 * **/
function addReveal() {
    $(".nav-bar__top .nav__item--01 > a").attr('data-open', 'homePopup').removeAttr("href");

    /**
     * Check for Why in box in mobile menu and remove href and add foundation popup modal
     */

    if ($('.mobile-nav .mobile-nav__item .mobile-nav__link:contains("Why In a Box")').length > 0) {
        $('.mobile-nav .mobile-nav__item .mobile-nav__link:contains("Why In a Box")').attr('data-open', 'homePopup').removeAttr("href");
    }
}

function initBulkOptions() {

    var $options = $('.bulk-option');

    if ($('.is-toggle-options').length == 0)
        $options.removeClass('hidden');

    $options.on('click mouseover touchend', function (e) {
            if (Foundation.MediaQuery.current == 'small')
                return;

            e.stopPropagation();

            $(this).addClass('is-hover');
        })
        .on('mouseout', function () {
            $(this).removeClass('is-hover');
        });

    //Display other bulk options
    $('.js-show-others').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $hiddenOptions = $this.siblings('.bulk-option.hidden '),
            $customQuote = $this.closest('.addtocart').siblings('.custom-quote');

        $hiddenOptions.each(function (i) {
            $(this).delay(100 * i)
                .fadeIn(250);
        });

        $customQuote.removeClass('hide');
        $this.addClass('hidden');
    });

    // Trigger ajax-cart
    // note: ajax-cart hasn't been changed. AJax still depends on the button id 'AddToCart'
    $('.js-trigger-add').on('click', function (e) {
        // e.preventDefault();

        var $this = $(this),
            $submitBtn;

        $this
            .attr('disabled', true)
            .addClass('is-adding');

        if ($('.bulk-dropdown').length) {
            $submitBtn = $this.closest('.bulk-dropdown').siblings('#AddToCart');

            //trigger submission of the related form
            $submitBtn.trigger('click');
            return;
        }

        //following lines are needed only for .bulk-list
        var radioId = $this.data('radio-id'),
            $bulkList = $this.closest('.bulk-list'),
            $radio;

        $submitBtn = $bulkList.siblings('#AddToCart');

        //to target the right radio, there are duplicated id
        $radio = $bulkList.find('#' + radioId);

        //check selected bulk option
        $radio.prop('checked', true);

        //trigger submission of the related form
        $submitBtn.trigger('click');
    });

    $('.js-close-bulk-list-reveal').on('click', function (e) {
        e.preventDefault();

        if (Foundation.MediaQuery.current == 'small') {
            $('#bulk-list-mobile').foundation('close');
            $('#custom-quote').foundation('open');
            return false;
        }

    });
}

function initUploadFile() {

    var $fields = $('.upload-field');

    var returnFileSize = function (number) {
        if (number < 1024) {
            return number + 'bytes';
        } else if (number >= 1024 && number < 1048576) {
            return (number / 1024).toFixed(1) + 'KB';
        } else if (number >= 1048576) {
            return (number / 1048576).toFixed(1) + 'MB';
        }
    }

    var isValidFile = function (fileSize, fileName) {

        var validExtensions = ['jpeg', 'jpg', 'png'],
            maxSize = 1048576 * 2, //2 MB
            errors = [];

        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);

        // check format
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            errors.push('Only .JPEG, .JPG and .PNG files are allowed');
        }

        // check size
        if (fileSize > maxSize) {

            fileSize = returnFileSize(fileSize);
            errors.push('File is too big (' + fileSize + '). Max size allowed is 2MB');
        }

        return errors;
    }

    var loadImage = function (event, name, fileName, fileSize) {
        var output = document.getElementById(name);

        output.src = URL.createObjectURL(event.target.files[0]);
        output.classList.add('uploaded');
        output.previousElementSibling.innerHTML = '<span class="upload-preview-name">' + fileName + '</span><span class="upload-preview-">' + fileSize + '</span><button class="button upload-preview-toggle js-remove-preview" type="button"> Remove </button>';
    }

    $fields.on('change', function (event) {

        var $this = $(this),
            name = $this.attr('name'),
            $loading = $this.parent().siblings('.upload-area-loading');

        //display overlay to prevent user to click on the area while uploading
        $loading.addClass('is-uploading');

        var fileName = event.target.files[0].name,
            fileSize = event.target.files[0].size,
            errors = isValidFile(fileSize, fileName);

        //prevent files to be uploaded, if not images
        if (errors.length > 0) {
            $loading
                .removeClass('is-uploading')
                .addClass('has-error')

            var $errorList = $loading
                .find('.upload-errors-list')
                .empty()

            for (var i = 0, l = errors.length; i < l; i++) {
                $errorList.append('<li>' + errors[i] + '</li>');
            }

            return false;
        }

        fileSize = returnFileSize(fileSize);

        loadImage(event, name, fileName, fileSize);

        $this.removeClass('empty');
        $loading.removeClass('is-uploading');
    });

    //Display details on hover
    $('.upload-preview-wrap').on('click mouseover touchend', function (e) {
            e.stopPropagation();

            $(this).addClass('show');
        })
        .on('mouseout', function () {
            $(this).removeClass('show');
        });

    //Remove
    $('.upload-preview-wrap').on('click', '.js-remove-preview', function (e) {
        e.stopPropagation();

        var $this = $(this),
            $img = $this.parent().siblings('img'),
            name = $img.attr('id'),
            $input = $('[name=' + name + ']');

        $this
            .parent()
            .html('')
            .parent()
            .removeClass('show');

        $img
            .attr('src', '')
            .removeClass('uploaded');

        $input.val('').addClass('empty');
    });

    //Trigger input change
    $('.js-upload-image').on('click touchend', function (e) {

        var $this = $(this),
            fields = $this.data('fields'),
            $input = $('.empty[name*=' + fields + ']').first();

        if ($input)
            $input.trigger('click');

        //avoid event to trigger twice on touch device
        if (e.type == 'touchend')
            $(this).off('click');
    });

    //close error
    $('.js-continue-uploading').on('click', function (e) {
        e.preventDefault();

        $(this)
            .closest('.upload-area-loading')
            .removeClass('has-error');
    })
}

$(document).ready(function () {
    /**
     * Remove clickable link and add attribute to 'Why in a box' in menu
     * **/
    addReveal();

    //init upload-file only if available
    if ($('.upload').length > 0) {
        initUploadFile();
    }

    //init bulk-option only if product page
    if ($('.template-product').length > 0) {
        initBulkOptions();
    }

    // on mobile the two sections are stacked. Need to display one or the other
    // depending on what link the user clicks on
    var $toggleFreshCoachReveal = $('[data-display-section]');
    if ($toggleFreshCoachReveal.length > 0) {

        var $reveal = $('#freshnessCoachPopup');

        $toggleFreshCoachReveal.on('click', function () {

            $('.popup-section').removeClass('selected');

            var sectionClass = $(this).data('display-section');

            $('.' + sectionClass).addClass('selected');

        });

        $reveal.on('click', '.close-button', function () {
            $reveal.find('.popup-container').scrollTop(0);
        })
    }


    /**
     * REMOVE CLICKABLE LINK FOR `GARDEN TYPE`
     **/

    $('.nav__bottom ul > li:first-of-type > a.site-nav__link').removeAttr("href");

    /**
     * REMOVE CLICKABLE LINK FOR `GARDEN TYPE`
     * **/

    $('.site-nav__dropdown .dropdown__inner li a').hover(function () {
        $value = $(this).closest(".site-nav__dropdown")[0].classList[1];
        $('.' + $value + ' .nav__image').css({
            "opacity": "0",
            "transition": "opacity .4s ease-in-out"
        });
        $('.nav__image.' + this.className).css({
            "opacity": "1"
        });
    });

    /**
     * Similar Products Owl Carousel | On Single Product page
     * **/

    $(".similar__products .grid--border").owlCarousel({

        dots: 'false',
        responsive: {
            0: {
                items: 1,
            },
            1024: {
                items: 4
            }
        }
    });

    $(".feature__tab").tabs();

    $('.search-icon').on('click', function () {
        $(this).closest('.search-form').submit();
    });

    // TODO: Remove this function, and use foundations match height
    /**
     * Matchheight universal class
     * **/
    $('.match--height').matchHeight();

    /**
     * Product Collection Slider
     * **/

    $(".product__hover--image").owlCarousel({
        items: 1,
        dots: 'false',
        lazyLoad: true,
        nav: true,
        stagePadding: 0,
        margin: 1,
        navText: ["<img src='{{ 'arrow-left.svg' | asset_url }}'>", "<img src='{{ 'arrow-right.svg' | asset_url }}'>"],
        onLoadLazy: function (elem) {
            $(elem.element[0].parentElement.parentElement.parentElement).css("background-color", "#eee");
        },
        onLoadedLazy: function (elem) {
            $(elem.element[0].parentElement.parentElement.parentElement).css("background-color", "transparent");
        },
    });

    /**
     * Trigger next image on slider on hover else revert to 1st item | Collection Page
     * **/

    $('.main__product').hover(function () {
        $(this).find('.product__hover--image').trigger('next.owl.carousel');
    }, function () {
        $(this).find('.product__hover--image').trigger('to.owl.carousel', 0);
    });

    /**
     * Collection Header Toggle Content
     **/

    $('.collection__content a, a[data-toggle="product-description"]').click(function () {
        $(this).toggleClass('enabled');
        if ($(this).hasClass('enabled')) {
            $(this).html("close");
        } else {
            $(this).html("Read more");
        }
    });

    /**
     * Gets height on description before max-height is set.
     * **/

    $('.product-description').css("max-height", "100%");
    var ContentsHeight = $('.product-description').height();

    /**
     * If description is smaller than max-height set, hide toggleClass
     * **/

    $('.product-description').css("max-height", "");
    if ($('.product-description').height() >= ContentsHeight) {
        $('a[data-toggle="product-description"]').hide();
    }
});

$(window).load(function () {

    singleProductNav();

    $(".desktop--addcart input:radio[name=id]:first-of-type, .buy__cta input:radio[name=id]:first-of-type, .mobile--addcart input:radio[name=id]:first-of-type").attr('checked', true);

    if ($(".desktop--addcart input[type='radio']:checked, .mobile--addcart input[type='radio']:checked, .buy__cta input[type='radio']:checked").prop('disabled') == true) {
        $(".desktop--addcart button[type='submit'], .mobile--addcart button[type='submit'], .buy__cta button[type='submit']").prop('disabled', true).html("Sold Out");
    } else {
        $(".desktop--addcart button[type='submit'], .mobile--addcart button[type='submit'], .buy__cta button[type='submit']").prop('disabled', false).html("Add to Cart").append(' <img src="//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/addtocart.svg" />');
    }

    $(".product__details").owlCarousel({
        dots: false,
        center: true,
        loop: true,
        autoplay: true,
        responsive: {
            640: {
                items: 5,
                loop: false,
                center: false,
                autoplay: false,
            },
        }
    });

    /**
     * Get Height of h2, to set the max-height on paragraph
     * **/

    $('.collection__desc h2').prependTo('.collection__title');
    var colheight = $('.collection__title h2').height();

    /**
     * Gets the height of element before max-height is set
     * **/
    $('.collection__desc').css("max-height", "100%");
    var ContentsHeight = $('.collection__desc').height();

    /**
     * If max-height is larger than  element height, hide the toggleClass
     * **/

    if (colheight >= ContentsHeight) {
        $('a[data-toggle="collection__desc"]').hide();
    }

    /**
     * Set max-height to be the same size as h2
     * **/

    $('.collection__desc').css("max-height", colheight);

    /**
     * CAROUSEL SYNC
     * Owl Carousel DOM Elements
     */

    var carousel1 = '.js-carousel-1';
    var carousel2 = '.js-carousel-2';

    var owlCarousel1 = $(carousel1).owlCarousel({
        items: 1,
        autoHeight: true,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    });

    var owlCarousel2 = $(carousel2).owlCarousel({
        items: 4,
        margin: 10,
        dots: false
    });

    // Sync carousel & add current class
    carouselSyncCurrentClass();

    // On carousel change: Sync carousel & add current class
    owlCarousel1.on('changed.owl.carousel', function () {
        carouselSyncCurrentClass();
    });
    owlCarousel2.on('changed.owl.carousel', function (event) {
        carouselSyncCurrentClass();
    });

    // Thumbs switch click event.
    owlCarousel2.find('.item').click(function () {
        var itemIndex = $(this).parent().index();
        owlCarousel1.trigger('to.owl.carousel', itemIndex);
        carouselSyncCurrentClass();
    });

    function carouselSyncCurrentClass() {
        setTimeout(function () {
            var carousel1ActiveIndex = $('.js-carousel-1 .owl-item.active').index();
            $('.js-carousel-2 .owl-item').removeClass('current');
            var currentItem = $('.js-carousel-2 .owl-item:nth-child(' + (carousel1ActiveIndex + 1) + ')');
            currentItem.addClass('current');

            if (!currentItem.hasClass('active')) {
                if (currentItem.prevAll('.active').length > 0) {
                    owlCarousel2.trigger('next.owl.carousel');
                }
                if (currentItem.nextAll('.active').length) {
                    owlCarousel2.trigger('prev.owl.carousel');
                }
            }
        }, 100);
    }

    $(".single-article img").wrap("<div class='image-wrap'></div>");

    /**
     * Initialise Foundation
     */

    $(document).foundation();
});

var basicauth = "Basic amVyZW15K3BpYWJAbWFkZXdpdGhwbHkuY29tL3Rva2VuOmdZcmNsQldYaFNyTEZJdzZGNDNYYXd0Y3JUaWVPeXN3YTJjN2tEWFY=";
var articles = [];
var categories = [];

function showArticle(article) {
    $(".articles-diag #articles").addClass('hidden');
    var html = '<h4 class="row"><div class="column btn-wrap"><a class="btn-back"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a><a class="button btn-close">Close</a></div></h4><h4 class="text-center title">' + article.title + '</h4>';
    html += article.body;
    $(".articles-diag #article-content").html(html);

    $("a.btn-close").click(function () {
        $(".articles-diag").addClass('hidden');
        $(".articles-diag #articles").removeClass('hidden');
        $(".articles-diag #article-content").addClass('hidden');
        $(document).find('body').css('overflow', 'auto');
    });

    $("a.btn-back").click(function () {
        $(".articles-diag #articles").removeClass('hidden');
        $(".articles-diag #article-content").addClass('hidden');
    });

    $(".articles-diag #article-content").removeClass('hidden');
}

function buildArticles(articles, categoryTitle) {
    var html = '<h4 class="text-right"><a class="button btn-close">Close</a></h4>' + '<h4 class="text-center title">' + categoryTitle + '</h4>';
    for (var i = 0; i < articles.length; i++) {
        html += '<div class="article column large-12 medium-12 small-12" article-index=' + i + ' article-id="' + articles[i].id + '">' +
            '<h5 class="text-left">' + articles[i].name + '</h5>' +
            '</div>';
    }

    $("#articles").html(html);

    $(".articles-diag").removeClass('hidden');
    $(document).find('body').css('overflow', 'hidden');

    $("a.btn-close").click(function () {
        $(".articles-diag").addClass('hidden');
        $(document).find('body').css('overflow', 'auto');
    });

    $("#articles .article").click(function () {
        showArticle(articles[this.getAttribute('article-index')]);
    });
}

function getArticlesByCategoryId(id, categoryTitle) {
    $.ajax({
        type: "GET",
        url: "https://plantsinabox.zendesk.com/api/v2/help_center/en-us/categories/" + id + "/articles.json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", basicauth);
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (res) {
            articles = res.articles;
            buildArticles(articles, categoryTitle)
        }
    });
}

function sortByName(a, b) {
    return a.name > b.name ? 1 : a.name == b.name ? 0 : -1;
}

function getCategories() {
    $.ajax({
        type: "GET",
        url: "https://plantsinabox.zendesk.com/api/v2/help_center/en-us/categories.json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", basicauth);
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (res) {
            var catCalls = [];
            for (var i = 0; i < res.categories.length; i++) {
                catCalls.push($.ajax({
                    type: "GET",
                    url: res.categories[i].url,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", basicauth);
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (catres) {
                        categories.push(catres.category);
                        categories.sort(sortByName);
                    }
                }));
            }

            $.when.apply($, catCalls).done(function () {
                var html = '';
                var icons = [
                    "//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/icon-delivery.svg",
                    "//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/icon-media.svg",
                    "//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/icon-order-issues.svg",
                    "//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/icon-plant-care.svg",
                    "//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/icon-refunds.svg",
                    "//cdn.shopify.com/s/files/1/0248/6983/t/19/assets/icon-website-questions.svg"
                ];

                for (var i = 0; i < categories.length; i++) {
                    html += '<div class="category" category-id="' + categories[i].id + '">' +
                        '<div class="text-center"><p class="icon-image"><img src=' + icons[i] + '></p><h4>' +
                        categories[i].name + '</h4></div>' +
                        '</div>';
                }

                $("#categories").html(html);

                $(".category").click(function () {
                    var categoryId = this.getAttribute('category-id');
                    getArticlesByCategoryId(categoryId, $(this).find('h4').html());
                });
            });
        },
        error: function (err) {
            console.log(err);
        }
    });
}

$(document).ready(function () {
    var isValidForm = function (_requiredFields) {
        var errors = 0,
            field,
            field;

        for (var i = 0, j = _requiredFields.length; i < j; i++) {
            field = _requiredFields[i];
            $field = $("[name='" + field + "']");

            if ($field.val() == '') {
                $field.addClass('has-error');
                errors++;
            } else
                $field.removeClass('has-error');
        }

        return errors;
    }

    $("#assistant-form").submit(function (e) {
        e.preventDefault();

        var $formStatus = $('.form-status'),
            $btn = $('#assistant-form button[type="submit"]'),
            _requiredFields = ['fullName', 'email', 'postcode'],
            isUserGardenImgUploaded = $('[name="user-garden-1"]').val() === '' ? false : true;

        //prevent form to be submitted, if error
        if (isValidForm(_requiredFields) > 0) {
            $formStatus
                .addClass('has-error')
                .find('p')
                .text('One or more fields have an error. Please check and try again');
            return false;
        } else if (!isUserGardenImgUploaded) {
            $formStatus
                .addClass('has-error')
                .find('p')
                .text('Please add at least one image of your garden');
            return false;
        } else
            $formStatus.removeClass('has-error');

        $btn.attr('disabled', true);

        //prepare links
        var $links = $('.user-link')
        nLinks = $links.length,
            index = 1;

        for (var i = 0; i < nLinks; i++) {
            $links
                .eq(i)
                .attr('name', 'link-' + index)

            index++;
        }

        var form = $('#assistant-form')[0],
            formData = new FormData(form);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false, // Important!
            contentType: false,
            cache: false,
            url: $(this).attr("action"),
            data: formData,
            headers: {
                "Accept": "application/json"
            },
            success: function (response) {
                var $hero = $('.garden-assistant-hero');

                $('.garden-assistant-form').addClass('hide');

                $hero.addClass('full-height');

                $hero
                    .find('h1')
                    .text('Yes! Garden help is on it’s way.')

                $hero
                    .find('.rte')
                    .empty()
                    .append('<p>Our plant nerds are on the case. We’ll review the details you sent us and come back to you within 48 hrs with a game plan for your garden. Back to you real soon! </p>')
                    .append('<a class="button" href="/collections/all"> Keep Shopping</>');

                $('html, body').animate({
                    scrollTop: 0
                }, 800);
            },
            error: function (data) {
                $btn.attr('disabled', false);

                $formStatus
                    .addClass('has-error')
                    .find('p')
                    .text('Oops something went wrong');

                console.log('error');
            }
        });

    });

    var basicauth = "Basic amVyZW15K3BpYWJAbWFkZXdpdGhwbHkuY29tL3Rva2VuOmdZcmNsQldYaFNyTEZJdzZGNDNYYXd0Y3JUaWVPeXN3YTJjN2tEWFY=";

    $("#contact_form").submit(function () {
        var name = $("#ContactFormName").val();
        var email = $("#ContactFormEmail").val();
        var subject = $("#ContactFormSubject").val();
        var msg = $("#ContactFormMessage").val();

        //extra fields available on custom quote
        if ($('.custom-quote').length) {
            var phone = 'Phone: ' + $('#ContactFormPhone').val(),
                postcode = 'Postcode: ' + $('#ContactFormPostcode').val();

            msg = 'Full Name: ' + name + '\n' + phone + '\n' + postcode + '\n' + 'Message: ' + msg;
        }

        var $formStatus = $('.form-status'),
            _requiredFields = ['contact[email]', 'contact[subject]', 'contact[body]'];

        //prevent form to be submitted, if error
        if (isValidForm(_requiredFields) > 0) {
            $formStatus
                .addClass('has-error')
                .find('p')
                .text('One or more fields have an error. Please check and try again');
            return false;
        } else
            $formStatus.removeClass('has-error');

        $.ajax({
            type: "POST",
            url: "https://plantsinabox.zendesk.com/api/v2/requests.json",
            data: JSON.stringify({
                "request": {
                    "requester": {
                        "name": name,
                        "email": email
                    },
                    "subject": subject,
                    "comment": {
                        "body": msg
                    }
                }
            }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", basicauth);
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {

                $('.contact-form-fields').addClass('hide');

                $('.contact-form-btn')
                    .removeClass('alert')
                    .addClass('success')
                    .text('Message Sent')
                    .attr("disabled", true);

                $formStatus
                    .addClass('success')
                    .find('p')
                    .text('Your email has been sent and a plant nerd is on the case. We’ll get back to you shortly')
            },
            error: function (data) {
                console.log('error');
            },
            dataType: 'json'
        });

        return false;
    });

    $("#search-articles").keypress(function (evt) {
        if (evt.charCode == 13) {
            $.ajax({
                type: "GET",
                url: "https://plantsinabox.zendesk.com/api/v2/help_center/articles/search.json?query=" + $(this).val(),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", basicauth);
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                dataType: 'json',
                success: function (search_results) {
                    console.log(search_results);
                    buildArticles(search_results.results, "Search Results");
                }
            });
        }
    })

    getCategories();
});
