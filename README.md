A light readme for Plants in a Box templating and Deployment.

# Local Development
A `package.json` has been created for the required node modules. Additional settings for Shopify uploading can be found in the `config.yml` and auto refreshing inside of `gulpfile.js`.

# Deployment

## GIT
*Gitflow* is the preferred workflow for local, to production protection. This ensures local work is never completed on `master` which will be a protected branch in the future.

## Production Auto Deploy
The template will auto-deploy from the `master` branch to the production template via Buddy CI.
