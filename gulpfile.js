var gulp = require("gulp");
var sass = require("gulp-sass");
var uglify = require("gulp-uglify");
var pump = require("pump");
var rename = require("gulp-rename");
var concat = require("gulp-concat");
var flatten = require("gulp-flatten");
var autoprefixer = require("gulp-autoprefixer");
var sourcemaps = require("gulp-sourcemaps");
var watch = require("gulp-watch");
var shopify = require("gulp-shopify-upload");
var browserSync = require("browser-sync").create();

// Folders
var paths = {
	sass: ["source/**/*.scss"],
	// sassconcat: ['src/globals/*.scss', 'src/blocks/*.scss', '!src/core.scss', '!src/_concat.scss', 'src/components/*.scss', 'src/pages/*.scss'],
	uglify: ["source/js/*js"]
};

gulp.task("sass-concat", function() {
	return gulp
		.src(paths.sassconcat)
		.pipe(concat("_concat.scss"))
		.pipe(
			flatten({
				includeParents: 0
			})
		)
		.pipe(gulp.dest("styles"));
});

// SASS
gulp.task("sass", function() {
	return (gulp
			.src(paths.sass)
			// .pipe(sourcemaps.init())
			.pipe(
				sass({
					outputStyle: "compressed"
				}).on("error", sass.logError)
			)
			// .pipe(sourcemaps.write())
			.pipe(
				autoprefixer({
					browsers: [
						"ie >= 10",
						"ie_mob >= 10",
						"ff >= 30",
						"chrome >= 34",
						"safari >= 7",
						"opera >= 23",
						"ios >= 7",
						"android >= 4.4",
						"bb >= 10"
					],
					cascade: false
				})
			)
			.pipe(
				flatten({
					includeParents: 0
				})
			)
			.pipe(gulp.dest("assets"))
			.pipe(browserSync.stream()) );
});

// UGLIFY
gulp.task("compress", function(cb) {
	pump(
		[
			gulp.src(paths.uglify),
			uglify(),
			concat("functions.js.liquid"),
			gulp.dest("assets")
		],
		cb
	);
});

//id for develop theme: 6745325598
// id to use for feature branches: 156112773
// Shopify Theme
gulp.task("shopify", function() {
	return watch("./+(assets|layout|config|snippets|templates|locales)/**").pipe(
		shopify(
			"47d8eb45b1df5132bc8c1620307e5394",
			"bdb812d935ee2d70504bce67dcf8b09d",
			"plants-in-a-box.myshopify.com",
			"6745325598"
		)
	);
});

// Watch
gulp.task("watch", function() {
	gulp.watch(paths.sass, ["sass"]);
	gulp.watch(paths.uglify, ["compress"]);
	gulp.watch("./**/*.liquid").on("change", browserSync.reload);

	browserSync.init({
		reloadDelay: 3000,
		proxy: "https://plants-in-a-box.myshopify.com?preview_theme_id=6745325598"
	});
});

// Default gulp task
gulp.task("default", ["watch", "shopify"]);
